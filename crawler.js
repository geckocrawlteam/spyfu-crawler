const crawlutil=require("./crawlutil.js");

module.exports=async function(crawl) {
	let page=await crawl.getPage();

	let index=0;
	for (let url of crawl.input.urls) {
		await crawlutil.deleteAllCookies(page);
		crawl.log("Checking: "+url);

		await page.goto("https://www.spyfu.com/overview/domain?query="+encodeURIComponent(url));
		await crawlutil.injectJquery(page);

		let values=await page.evaluate(()=>{
			return {
				"organicKeywords": jQuery(".dashboard-left span[data-test='valueA']").text(),
				"seoClicks": jQuery(".dashboard-left span[data-test='valueB']").text(),
				"paidKeywords": jQuery(".dashboard-right span[data-test='valueA']").text(),
				"ppcClicks": jQuery(".dashboard-right span[data-test='valueB']").text()
			}
		});

		crawl.result(values);

		index++;
		crawl.progress(100*index/crawl.input.urls.length);
	}
};